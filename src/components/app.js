import React from 'react'
import Input from './input/input'

export default class App extends React.Component {
  render () {
    return (
      <div>
        <div className="form-wrapper">
          <form>
            <Input type="text" label="Name" />
            <Input type="text" label="Age" />
            <Input type="text" label="Position" />
            <Input type="text" label="Department" />
            <div className="drop-area">
              <input type="file" name="images" id="images" required="required" multiple="multiple" />
              <div className="file-response">
                <div className="success">Your photograph is selected</div>
                <div className="default">Drag and drop the photograph</div>
              </div>
            </div>
            <button className="save">Save</button>
            <button className="cancel">Cancel</button>
          </form>
        </div>
        <footer>
          <a href="#">About Us</a>
          <a href="#">Privacy Policy</a>
          <a href="#">Copyright Message</a>
        </footer>
      </div>
    )
  }
}
