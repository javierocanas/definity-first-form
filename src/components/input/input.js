import React, { Component } from 'react';
import $ from 'jquery';
export default class Login extends Component {
    componentDidMount() {
        $('input').on('focus', function() {
            $(this).parent().addClass('is-focused');
        });
        $('input').on('input', function() {
            if ($(this).val() == '') {
                $(this).parent().removeClass('is-active');
            } else {
                $(this).parent().addClass('is-active');
            }
        });
        $('input').on('blur', function() {
            $(this).parent().removeClass('is-focused');
            if ($(this).val() == '') {
                $(this).parent().removeClass('is-active');
            }
        });
        $('input').each(function() {
            if ($(this).val() == '') {
                $(this).parent().removeClass('is-active');
            } else {
                $(this).parent().addClass('is-active');
            }
        });
    }
    render() {
        const style = this.props.search
            ? "float-label search"
            : "float-label";
        return (
            <div className={style}>
                <input placeholder={this.props.label} type={this.props.type} id={this.props.id}/>
                <label htmlFor={this.props.id}>{this.props.label}</label>
            </div>
        )
    }
}